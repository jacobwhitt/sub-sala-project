import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import { URL } from './config'
import Navbar from './components/Navbar'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Contact from './pages/Contact'
import AdminPortal from './pages/AdminPortal'
import HostPortal from './pages/HostPortal'
import SpaceCreateForm from './pages/SpaceCreateForm'
import UpdateSpaceForm from './pages/UpdateSpaceForm'
import ImageUploader from './components/ImageUploader'
import UserPortal from './pages/UserPortal'
import UserAccount from './pages/UserAccount'
import Search from './pages/Search'
import Space from './pages/Space'
import ReservationPage from './pages/Reservations/ReservationPage'
import HostReservationPage from './pages/Reservations/HostReservations'
import UserReservationPage from './pages/Reservations/UserReservations'
import Stripe from './components/stripe'
import PaymentSuccess from './pages/stripe/payment_success'
import PaymentError from './pages/stripe/payment_error'
import moment from 'moment'


function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [isAdmin, setIsAdmin] = useState(false)
  const [isHost, setIsHost] = useState(false)
  const [singlespace, setSingleSpace] = useState([])
  const [spaces, setSpaces] = useState([])
  const [locations, setLocations] = useState([])
  const [type, setType] = useState([])
  const [time, setTime] = useState([])
  const [city, setCity] = useState('')
  const [date, setDate] = useState('')
  const [filteredSpaces, setFilteredSpaces] = useState([])
  const [typemodel, setTypeModel] = useState([
    { name: 'meal', checked: false },
    { name: 'art', checked: false },
    { name: 'music', checked: false }
  ])
  const [timemodel, setTimeModel] = useState([
    { name: 'morning', checked: false },
    { name: 'afternoon', checked: false },
    { name: 'night', checked: false }
  ])
  const resetHome = () => {
    setType([]);
    setTime([]);
    setCity('default');
    setFilteredSpaces([])
    setTypeModel([
      { name: 'meal', checked: true },
      { name: 'art', checked: window.localStorage.getItem('art') === true ? true : false },
      { name: 'music', checked: window.localStorage.getItem('music') === true ? true : false }
    ])
    setTimeModel([
      { name: 'morning', checked: window.localStorage.getItem('morning') === true ? true : false },
      { name: 'afternoon', checked: window.localStorage.getItem('afternoon') === true ? true : false },
      { name: 'night', checked: window.localStorage.getItem('night') === true ? true : false }
    ])
    for (var i in window.localStorage) {
      if (typeArr.includes(i) || timeArr.includes(i)) {
        window.localStorage.removeItem(i);
      }
    }
    console.log('everything reset in home container')
  }


  //? LOGIN/LOGOUT TOKEN VERIFICATION
  const token = JSON.parse(localStorage.getItem('token'))
  useEffect(() => {
    const verify_token = async () => {
      if (token === null) { return setIsLoggedIn(false) }
      try {
        Axios.defaults.headers.common['Authorization'] = token
        const response = await Axios.post(`${URL}/users/verify_token`);
        // console.log('useEffect: ', response.data);
        response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
        response.data.admin ? setIsAdmin(true) : setIsAdmin(false);
        response.data.host ? setIsHost(true) : setIsHost(false);
      }
      catch (error) {
        console.log(error);
      }
    }
    verify_token();
  }, [token]);

  const login = (token) => {
    localStorage.setItem('token', JSON.stringify(token))
    setIsLoggedIn(true);
  }

  const logout = () => {
    if (!isLoggedIn) { return }
    localStorage.removeItem('token');
    window.localStorage.clear();
    Axios.defaults.headers.common['Authorization'] = '';
    setIsAdmin(false);
    setIsLoggedIn(false);
    setIsHost(false);
  }

  //? FIND ALL SPACES FOR SEARCH FILTERS
  useEffect(() => {
    find_spaces();
  }, []);

  const find_spaces = async () => {
    try {
      const response = await Axios.get(`${URL}/spaces/findall`);
      // console.log('App.js FIND SPACES', response.data)
      setSpaces([...response.data.spaces])
    }
    catch (error) {
      console.log(error);
    }
  }


  //? ARR USED TO SORT/DISPLAY INFO IN SAME ORDER ACROSS WEBSITE
  var typeArr = ['meal', 'art', 'music']
  var timeArr = ['morning', 'afternoon', 'night']
  var tempTypes = []
  var tempTimes = []

  var order = (arg1, arg2) => {
    // console.log('App.js order func',arg1, arg2)
    var temp = ''
    arg2.forEach(item => arg1.includes(item) ? temp += (item + ' ') : null)
    return temp
  }

  //? HANDLES TYPES OF EVENTS CHECKBOXES IN SEARCH
  const handleType = (e) => {
    // console.log(e.target.name)
    let temp = type
    let index = temp.findIndex(ele => ele === e.target.name)
    if (index === -1) {
      temp.push(e.target.name)

    } else {
      temp.splice(index, 1)
      window.localStorage.removeItem(`${e.target.name}`)
    }

    type.forEach((item, idx) => {
      if (temp.includes(item) && item === 'meal') { return tempTypes.unshift(item), window.localStorage.setItem(`${item}`, 'true'); }

      if (temp.includes(item) && item === 'art' && !temp.includes('music')) { return tempTypes.push(item), window.localStorage.setItem(`${item}`, 'true'); }
      else if (temp.includes(item) && item === 'art' && temp.includes('music')) { return tempTypes.splice(1, 0, item), window.localStorage.setItem(`${item}`, 'true'); }

      if (temp.includes(item) && item === 'music') { return tempTypes.push(item), window.localStorage.setItem(`${item}`, 'true'); }
    });

    console.log(tempTypes)
    // debugger
    setType([...type], [...tempTypes])
    // console.log('types of events', type)
  }

  //? HANDLES TIME OF DAY CHECKBOXES IN SEARCH
  const handleTime = (e) => {
    // console.log(e.target.name)
    let temp = time
    let index = temp.findIndex(ele => ele === e.target.name)
    if (index === -1) {
      temp.push(e.target.name)
    } else {
      temp.splice(index, 1)
      window.localStorage.removeItem(`${e.target.name}`)
    }

    time.forEach((item, idx) => {
      if (temp.includes(item) && item === 'morning') { return tempTimes.unshift(item), window.localStorage.setItem(`${item}`, 'true'); }

      if (temp.includes(item) && item === 'afternoon' && !temp.includes('night')) { return tempTimes.push(item), window.localStorage.setItem(`${item}`, 'true'); }
      else if (temp.includes(item) && item === 'afternoon' && temp.includes('night')) { return tempTimes.splice(1, 0, item), window.localStorage.setItem(`${item}`, 'true'); }

      if (temp.includes(item) && item === 'night') { return tempTimes.push(item), window.localStorage.setItem(`${item}`, 'true'); }
    });

    console.log(tempTimes)
    setTime([...time], [...tempTimes])
    // console.log('times of events', time)
  }

  //? HANDLES SELECTED DATE IN CALENDAR API
  const handleDate = (e) => {
    console.log('handleDate App.js', e._d)
    var localTime;
    if (e._d) {
      localTime = moment(e._d).format('YYYY-MM-DD')
      setDate(localTime)
    }
    else {
      setDate(date)
    }

  }

  var dateSelected = date //? used in the displaySearch function due to variable confusion


  const displaySearch = async () => {

    let tempArr = []
    spaces.map((item, idx) => {
      console.log(item)
      if (city === item.locationCity) {
        tempArr.push(item)
      }
    })


    let filtered = []
    //! ITERATE IN ARR OF SPACES FOR TYPES
    tempArr.map((item, idx) => {
      var found = false
      item.type.map((item2, idx2) => {
        if (typeArr.includes(item2)) {
          found = true
        }
      })
      if (found) {
        filtered.push(item)
      }
    })
    // console.log('spaces filtered by type', tempArr)

    //! ITERATE IN ARR OF SPACES MATCHING TYPE FOR MATCHING TIMES
    tempArr.map((item, idx) => {
      var found2 = false
      item.timeOfDay.map((item2, idx2) => {
        if (timeArr.includes(item2)) {
          found2 = true
        }
      })
      if (found2 && !tempArr.includes(item)) {
        filtered.push(item)
      }
    })
    // console.log('spaces filtered by time', filtered)

    //! ITERATE IN ARR OF FILTERED SPACES TO MATCH DATE AVAILABILITY
    // debugger
    console.log('date after debugger before crash', date)
    filtered.map((item, idx) => {
      console.log('item in date filtering', item)
      var found3 = false
      item.events.map((item3, idx3) => {
        if (item3.date.includes(dateSelected)) {
          found3 = true
        }
      })
      if (found3) {
        filtered.splice(idx, 1)
      }
    })

    //! ADD PHOTOS TO THE SPACES
    const ids = filtered.map(space => space._id)
    const response = await Axios.post(`${URL}/fotos/fetchspaceimages`, { ids })
    console.log('res.data.photos in APP.js filtered search photos', response.data.photos)

    response.data.photos.map((item, idx) => {
      item.original = `${URL}/assets/${item.filename}`
      item.thumbnail = `${URL}/assets/${item.filename}`
    })

    const tempSpaces = []
    filtered.map((item, idx) => {
      console.log('item in photo adding final push', item)
      tempSpaces.push({
        ...item,
        photos: response.data.photos.filter(ele => ele.typeId === item._id)
      })
    })
    console.log('temp =>', tempSpaces)
    setFilteredSpaces(tempSpaces)
  }

  // console.log('locations in app.js for search', locations)

  return (
    <div>

      <Router>
        <Navbar isLoggedIn={isLoggedIn} isAdmin={isAdmin} isHost={isHost} logout={logout} />

        <Route path='/register' component={Register} />

        <Route path='/login' render={props => <Login login={login} isAdmin={isAdmin} isHost={isHost}  {...props} />} />

        <Route path='/home' render={props => {
          return <Home {...props} resetHome={resetHome} spaces={spaces} locations={locations} setLocations={setLocations} handleType={handleType} handleTime={handleTime} date={date} handleDate={handleDate} setDate={setDate} tempTypes={tempTypes} tempTimes={tempTimes} type={type} setType={setType} time={time} setTime={setTime} city={city} setCity={setCity} order={order} displaySearch={displaySearch} />
        }} />

        <Route path='/search' render={props => {
          return <Search {...props} spaces={spaces} locations={locations} setLocations={setLocations} handleType={handleType} handleTime={handleTime} handleDate={handleDate} date={date} setDate={setDate} type={type} setType={setType} typemodel={typemodel} time={time} setTime={setTime} timemodel={timemodel} city={city} setCity={setCity} order={order} displaySearch={displaySearch} filteredSpaces={filteredSpaces} />
        }} />

        <Route path='/adminportal' render={props => {
          return isAdmin
            ? <AdminPortal logout={logout} isAdmin={isAdmin} {...props} /> : <Redirect to={'/home'} />
        }} />

        <Route path='/user' render={props => {
          return isLoggedIn && (!isHost || !isAdmin)
            ? <UserPortal {...props} /> : <Redirect to={'/home'} />
        }} />

        <Route path='/useraccount' render={props => {
          return isLoggedIn || isAdmin
            ? <UserAccount {...props} isHost={isHost} /> : <Redirect to={'/home'} />
        }} />

        <Route path='/host' render={props => {
          return isHost || isAdmin
            ? <HostPortal {...props} isAdmin={isAdmin} isHost={isHost}
            /> : <Redirect to={'/home'} />
        }} />

        <Route path='/spacecreateform' render={props => {
          return <SpaceCreateForm {...props} isAdmin={isAdmin} />
        }} />

        <Route path='/imageuploader/:_id/:type' component={ImageUploader} />

        <Route path='/updatespaceform' render={props => {
          return <UpdateSpaceForm {...props} isAdmin={isAdmin} />
        }} />

        <Route path='/singlespace/:_id' render={props => {
          return <Space {...props} typeArr={typeArr} timeArr={timeArr} order={order} date={date} />
        }} />

        <Route path='/reservationpage' render={props => {
          return <ReservationPage {...props} order={order} />
        }} />

        <Route path='/hostreservations' render={props => {
          return <HostReservationPage {...props} />
        }} />

        <Route path='/userreservations' render={props => {
          return <UserReservationPage {...props} />
        }} />

        <Route exact path="/stripe" render={props => <Stripe {...props} />} />

        <Route exact path="/payment/success" render={props => <PaymentSuccess {...props} find_spaces={find_spaces} isLoggedIn={isLoggedIn} isHost={isHost} />} />

        <Route exact path="/payment/error" render={props => <PaymentError {...props} />} />

        <Route path='/contact' component={Contact} />

      </Router>
    </div>
  );
}

export default App;