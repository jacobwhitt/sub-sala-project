const URL = window.location.hostname === `localhost` ?
    `http://localhost:3002` :
    `http://159.89.1.248`

export {
    URL
}