import React, { Component } from 'react';
import moment from 'moment';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { SingleDatePicker } from 'react-dates';


export default class CalendarReservations extends Component {

    static getDerivedStateFromProps(props, state) {
        const BLOCKED_DATES = [];

        console.log('props in getDerivedState', props.date)
        props.events.forEach((item, idx) => {
            BLOCKED_DATES.push(moment(item.date))
        })
        return { blockedDates: BLOCKED_DATES }

    }


    constructor(props) {
        super(props);
        console.log('props in CalendarReservations', this.props)
        this.state = {
            focusedInput: false,
            date: this.props.date ? moment(this.props.date) : moment(),
            fullscreen: false,
            direction: 'left',
            dateFormat: 'DD/MM/YYYY',
            small: true,
            block: false,
            orientation: 'horizontal',
            numMonths: 1,
            minimumNights: 0
        };

        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleFocusChange = this.handleFocusChange.bind(this);
        this.handleChangeFullscreen = this.handleChangeFullscreen.bind(this);
        this.handleChangeDirection = this.handleChangeDirection.bind(this);
        this.handleChangeDateFormat = this.handleChangeDateFormat.bind(this);
        this.handleIsDayBlocked = this.handleIsDayBlocked.bind(this);
    }

    handleDateChange({ date }) {
        console.log('start/endDate', date)
        this.setState({ date });
    }

    handleFocusChange({ focused }) {
        this.setState({ focusedInput: focused });
    }

    handleChangeFullscreen() {
        this.setState({ fullscreen: !this.state.fullscreen });
    }

    handleChangeDirection(e) {
        this.setState({ direction: e.target.value });
    }

    handleChangeDateFormat(e) {
        this.setState({ dateFormat: e.target.value });
    }

    handleIsDayBlocked(day) {
        return this.state.blockedDates.filter(d => d.isSame(day, 'day')).length > 0;
    }



    render() {
        // console.log('coming from render in CalendarReservations')
        const blockedDates = this.state.blockedDates

        return (
            <div className="App" >
                <SingleDatePicker
                    date={this.state.date} // momentPropTypes.momentObj or null
                    isDayBlocked={this.handleIsDayBlocked}
                    onDateChange={date => this.setState({ date: date }, () => this.props.dateHandler(date))} // PropTypes.func.isRequired
                    focused={this.state.focused} // PropTypes.bool
                    numberOfMonths={1}
                    onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
                    id="your_unique_id" // PropTypes.string.isRequired,
                />
            </div>
        );
    }
}