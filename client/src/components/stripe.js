import React from "react";
import Checkout from "../pages/stripe/checkout";
import { StripeProvider, Elements } from "react-stripe-elements";
import { URL } from '../config'


const Stripe = props => {
  console.log('public key in checkout', process.env.REACT_APP_STRIPE_PUBLIC_KEY)

  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Checkout {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
