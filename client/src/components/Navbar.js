import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'


const Navbar = (props) => {

  const calllogout = () => {
    props.logout();
    window.localStorage.clear();
    props.history.push('/home');
  }

  return (
    <div className='navbar' >

      {!props.isLoggedIn ?

        <>
          <div className='homebutton'>
            <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/home"}
            >Home</NavLink>
          </div>
          
          <div className='contactbutton'>
            <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/Contact"}
            >Contact</NavLink>
          </div>

          <div className='loginNavButton'>
            <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/login"}
            >Login</NavLink>
          </div>

          <div className='registerNavButton'>
            <NavLink
              exact
              style={styles.default}
              activeStyle={styles.active}
              to={"/register"}
            >Register  </NavLink>
          </div>

          
          
        </>
        : null
      }

      {
        props.isLoggedIn && !props.isAdmin && !props.isHost ?
          <>
            <div className='homebutton'>
              <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={"/home"}
              >Home</NavLink>
            </div>

            <div className='contactbutton'>
              <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={"/Contact"}
              >Contact</NavLink>
            </div>

            <div className='userbutton'>
              <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={"/user"}
              >User Portal</NavLink>
            </div>

            <div className='logoutbutton'>
              <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={'/home'}
                onClick={calllogout}
              >Log out</NavLink>
            </div>
          </>
          :
          props.isLoggedIn && props.isAdmin ?
            <>
              <div className='homebutton'>
                <NavLink
                  exact
                  style={styles.default}
                  activeStyle={styles.active}
                  to={"/home"}
                >Home</NavLink>
              </div>

              <div className='adminbutton'>
                <NavLink
                  exact
                  style={styles.default}
                  activeStyle={styles.active}
                  to={"/adminportal"}
                >Admin Portal</NavLink>
              </div>


              <div className='logoutbutton'>
                <NavLink
                  exact
                  style={styles.default}
                  activeStyle={styles.active}
                  to={'/home'}
                  onClick={calllogout}
                >Log out</NavLink>
              </div>

            </>
            : props.isLoggedIn && props.isHost ?

              <>
                <div className='homebutton'>
                  <NavLink
                    exact
                    style={styles.default}
                    activeStyle={styles.active}
                    to={"/home"}
                  >Home</NavLink>
                </div>

                <div className='contactbutton'>
                  <NavLink
                    exact
                    style={styles.default}
                    activeStyle={styles.active}
                    to={"/Contact"}
                  >Contact</NavLink>
                </div>

                <div className='hostbutton'>
                  <NavLink
                    exact
                    style={styles.default}
                    activeStyle={styles.active}
                    to={"/host"}
                  >Host Portal</NavLink>
                </div>

                <div className='logoutbutton'>
                  <NavLink
                    exact
                    style={styles.default}
                    activeStyle={styles.active}
                    to={'/home'}
                    onClick={calllogout}
                  >Log out</NavLink>
                </div>
              </>
              : null
      }
    </div>
  )
}

export default withRouter(Navbar);

const styles = {
  active: {
    color: "deepskyblue"
  },
  default: {
    textDecoration: "none",
    color: "seashell"
  }
};
