import React, { useState } from "react";


const Image = props => {
    const [dispMessage, setDispMessage] = useState(false);
    console.log('props in image container', props)

    return !dispMessage ? (
        <div className="image_container">
            <img
                className="image"
                src={props.item.imagePreviewUrl}
                alt="test" />
            <h2 className="delete_x" onClick={() => setDispMessage(true)}>
                X</h2>
        </div>
    ) : (
            <div className="image_container i_c_ex">
                <p className="message_text">Do you want to</p>
                <p className="message_text">remove this image?</p>
                <div className="buttons_container">
                    <button
                        className="yes_button"
                        onClick={() => {
                            setDispMessage(false);
                            props.delete_image(props.idx, props.item._id, props.item.filename);
                        }}>Yes
                    </button>
                    <button className="no_button" onClick={() => setDispMessage(false)}>
                        No</button>
                </div>
            </div>
        );
};

export default Image;


