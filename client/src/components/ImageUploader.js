import React, { useState, useEffect } from "react";
import Axios from "axios";
import { Progress } from "reactstrap";
import ExistingImages from "./ExistingImages";
import Image from "./Image";
import { URL } from '../config'

function ImageUploader(props) {
    const [selectedFile, setSelectedFile] = useState(null);
    const [loaded, setLoaded] = useState(0);
    const [images, setImages] = useState([]);
    const [existing, setExisting] = useState([])
    const [localimages, setLocalImages] = useState([]);
    const [progressBar, setProgressBar] = useState({
        visible: false
    })

    useEffect(() => {
        console.log('local images from useEffect', localimages)

        const fetchImagesBy = async () => {
            try {
                console.log(props.match.params)
                let ids = props.match.params._id
                const response = await Axios.post(`${URL}/fotos/fetchimagesforhost/${ids}`);
                setExisting(response.data.photos)
            }
            catch (error) {
                console.log(error);
            }
        }
        fetchImagesBy();

    }, []);

    const delete_image = async (idx, _id, filename) => {
        try {
            const response = await Axios.delete(
                `${URL}/fotos/delete_image/${_id}/${filename}/${props.match.params._id}`

                //? fotos/delete/fotoID/filename/spaceID
            );
            console.log("response.data.message =", response.data.message);
            const temp = existing;
            existing.splice(idx, 1);
            setExisting([...temp]);
        } catch (error) {
            console.log("error ==>", error);
        }
    };

    //! checking that the file is an image
    const checkMimeType = event => {
        //getting file object
        let files = event.target.files;
        //define message container
        let err = [];
        // list allow mime type
        const types = ["image/png", "image/jpeg", "image/gif"];
        // loop access array
        for (var x = 0; x < files.length; x++) {
            // compare file type find doesn't matach
            if (types.every(type => files[x].type !== type)) {
                // create error message and assign to container
                err[x] = files[x].type + " is not a supported format\n";
            }
        }
        for (var z = 0; z < err.length; z++) {
            // if message not same old that mean has error
            // discard selected file
            event.target.value = null;
        }
        return true;
    };

    //! checking that only one file is selected
    const maxSelectFile = event => {
        let files = event.target.files;
        if (files.length > 1) {
            const msg = "Only 1 image can be uploaded at a time";
            event.target.value = null;
            console.log(msg);
            return false;
        }
        return true;
    };

    //! checking the filesize
    const checkFileSize = event => {
        let files = event.target.files;
        let size = 500000000;
        // 500 kbs
        let err = [];
        for (var x = 0; x < files.length; x++) {
            if (files[x].size > size) {
                err[x] = files[x].type + "is too large, please pick a smaller file\n";
            }
        }
        for (var z = 0; z < err.length; z++) {
            // if message not same old that mean has error
            // discard selected file
            console.log(err[z]);
            event.target.value = null;
        }
        return true;
    };

    //! putting image to state onChange
    const onChangeHandler = event => {
        var files = event.target.files;
        console.log(files)
        if (maxSelectFile(event) && checkMimeType(event) && checkFileSize(event)) {
            // if return true allow to setState
            setSelectedFile([...files]);
            console.log(files)
            setLoaded(0);
        }
        event.target.value = ''
    };

    //! sending file to the STATE onClick
    const handleImageChange = (e) => {
        let reader = new FileReader();
        let file = selectedFile[0];
        console.log('selFil', selectedFile)
        console.log('selFil[0]', selectedFile[0])
        console.log('file inside handleImageChange', file)

        reader.onloadend = () => {
            setLocalImages([...localimages, {
                file: file,
                imagePreviewUrl: reader.result
            }]);
        }
        reader.readAsDataURL(file)
        setSelectedFile(null);
    }

    //! sending file to the SERVER onClick
    const submitHandler = () => {
        setProgressBar({ visible: true })
        const data = new FormData();
        const files = []
        localimages.forEach(ele => files.push(ele.file))
        console.log('====> files', files)
        for (var x = 0; x < files.length; x++) {
            // files[x].element = props.match.params.type
            // files[x].elementID = props.match.params._id
            //? each loop adds one of the img files to the data request sent to the server
            data.append("file", files[x]);
            // we can add more data here that can be eventually retrieved
            // from the body object
        }
        Axios
            .post(`${URL}/fotos/upload/${props.match.params._id}/${props.match.params.type}`, data, {
                onUploadProgress: ProgressEvent => {
                    setLoaded((ProgressEvent.loaded / ProgressEvent.total) * 100);
                }
            })
            .then(res => {
                //  then print response status
                console.log("upload success", res.data);
                setLocalImages([]);
                setTimeout(() => {
                    alert('Photos uploaded correctly!')
                    props.history.push('/host')
                }, 1000)
            })
            .catch(err => {
                // then print response status
                console.log(err);
            });
    };


    //! RESETS UPLOAD WIDGET
    window.addEventListener("beforeunload", function (e) {
        setImages(images.length = 0)
    }, false);

    return (
        <>
            <div>
                <button className='backButtonSpaceImages' onClick={() => { props.history.push('/host') }}>Back</button>
            </div>
            <div className="main_container">

                <div className="offset-md-3 col-md-6">
                    
                    <div className="form-group files">
                        <label>Upload Your File </label>
                        <input
                            type="file"
                            className="form-control"
                            multiple
                            onChange={onChangeHandler}
                        />
                    </div>


                    
                    {selectedFile != null ? (
                        <button
                            type="button"
                            className="btn btn-success btn-block"
                            onClick={handleImageChange}
                        >Upload</button>
                    ) : null}
                </div>



                <div className="bottom">
                    {existing.map((item, idx) => {
                        return (
                            <ExistingImages
                                key={idx}
                                item={item}
                                delete_image={delete_image}
                                idx={idx}
                            />
                        );
                    })}

                    {localimages.map((item, idx) => {
                        return (
                            <Image
                                key={idx}
                                item={item}
                                delete_image={delete_image}
                                idx={idx}
                            />
                        );
                    })}

                    {localimages.length >= 1 ? (
                        <button
                            type="button"
                            className="btn btn-success btn-block"
                            onClick={submitHandler}
                        >Submit Fotos</button>
                    ) : null}

                    {progressBar.visible ? (
                        <div className="uploadBar">
                            <Progress max="100" color="success" value={loaded}>
                                {Math.round(loaded, 2)}% </Progress>
                        </div>
                    ) : null}
                </div>

            </div>

            <form>

            </form>
        </>
    );
}

export default ImageUploader;


