import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'
import CalendarSearch from "../components/CalendarSearch"
import moment from 'moment'



const Home = (props) => {
    const [message, setMessage] = useState('')
    // console.log('Home.js props', props)

    //! FIND SPACES IN CHOSEN CITY FROM SEARCHBAR
    useEffect(() => {
        props.resetHome();
        const find_locations = async () => {
            try {
                const response = await Axios.get(`${URL}/spaces/findlocations`);
                props.setLocations([...response.data.locations])
            }
            catch (error) {
                console.log(error);
            }
        }
        find_locations();
    }, []);


    //? USERS CHOOSE FROM CITIES WITH AVAILABLE SPACES PERIOD
    const handleCity = (e) => {
        props.setCity(e.target.value)
        console.log('city in Home', e.target.value)
    }

    //? HANDLE CALENDAR DATA
    const dateHandler = (date) => {
        // console.log('DateHandler in HOME', date._d)
        props.setDate(date._d)
    }


    //? SUBMIT TO SEARCH
    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(props.type, props.time, props.city)
        if (props.type.length === 0 && props.time.length === 0) {
            alert('Please choose at least one from either option:\n • Type of event \n • Time of day')
        } else if (props.city === 'default') {
            alert('Please select a city')
        } else if (!props.date) {
            alert('Please select a date')
        }
        else {
            props.displaySearch();
            props.history.push('/search')
        }
    }

    return (
        <>
            <div className='homeTitle'>
                <h1>Welcome to SubSala</h1>
                <h3>Find a space to host your event!</h3>
            </div>


            {message ?
                <div className='homeSearchMessage'>{message}</div>
                : null
            }
            <div className='searchcontainer'>
                <form className='homeSearch' onSubmit={handleSubmit}>
                    <div className='switchesGroup'>
                        {/*//* TYPE OF EVENT SWITCHES */}
                        <div className='searchSwitches'>
                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='mealswitch' name='meal' type="checkbox"
                                            onChange={props.handleType} /> <span className="slider round"></span>
                                    </label> Dining
                                </label>
                            </div>

                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='artswitch' name='art' type="checkbox"
                                            onChange={props.handleType} />
                                        <span className="slider round"></span>
                                    </label> Art Showcase
                                </label>
                            </div>

                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='musicswitch' name='music' type="checkbox"
                                            onChange={props.handleType} />
                                        <span className="slider round"></span>
                                    </label> Music Event
                                </label>
                            </div>
                        </div>


                        {/*//? TIME OF EVENT SWITCHES */}
                        <div className='searchSwitches'>
                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='morningswitch' name='morning' type="checkbox" onClick={props.handleTime} /> <span className="slider round"></span>
                                    </label> Morning
                                </label>
                            </div>

                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='afternoonswitch' name='afternoon' type="checkbox" onClick={props.handleTime} />
                                        <span className="slider round"></span>
                                    </label> Afternoon
                                </label>
                            </div>

                            <div>
                                <label>
                                    <label className="switch">
                                        <input id='nightswitch' name='night' type="checkbox" onClick={props.handleTime} />
                                        <span className="slider round"></span>
                                    </label> Night
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className='searchRight'>
                        <CalendarSearch handleDate={props.handleDate}
                            date={props.date} />

                        <label htmlFor="city"></label>
                        <select id='city' onChange={handleCity}>
                            <option value='city'>Select a city...</option>
                            {props.locations.map((item, idx) => {
                                return <option key={idx} value={item}>{item}</option>
                            })}
                        </select>

                    </div>

                    <button className='searchButton'>Find a Sala!</button>
                </form>
            </div>

            <div className='splash'></div>
        </>
    )
}
export default Home