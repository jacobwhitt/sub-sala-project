import React, { useState } from 'react'
import moment from 'moment'
import ReactModal from 'react-modal';


const HostReservations = (props) => {
    console.log('props in hostreservations', props)
    let spaces = props.location.state.spaces

    const [showModal, setModal] = useState(false)
    const [events, setEvents] = useState({})

    const handleModal = () => {
        setModal(!showModal);
    }


    return (
        <div className='hostReservations'>
            <ReactModal
                isOpen={showModal}
                className="Modal"
                overlayClassName="Overlay"
                ariaHideApp={false}
                onRequestClose={handleModal}
                contentLabel="Minimal Modal Example">
                <h5>User email: {events.userEmail}</h5>
                <h5>Reservation date: {moment(events.date).format('LL')}</h5>
                <h5>Invoice #:{events.invoiceNum}</h5>
                <button onClick={handleModal}>Close</button>
            </ReactModal>

            <h3>These are the spaces with event reservations</h3>
            <div>
                <button className='backButton' onClick={() => { props.history.push('/host') }}>Back</button>
            </div>

            {spaces.map((item, idx) => {
                {/* console.log('space `item` in map', item) */ }
                if (item.events.length > 0) {
                    return <div key={idx} className='reservationContainer'>
                        <div className='singleSpace'>
                            <h3>{item.locationAddress}</h3>
                            <p>Events:</p>
                            <div className='eventReservationsGrid'>
                                {item.events.map((item2, idx2) => {
                                    return <div className='reservationModals' key={idx2}>
                                        <button onClick={() => { return handleModal(), setEvents({ userEmail: item2.userEmail, date: item2.date, invoiceNum: item2.invoiceNum }) }}>{moment(item2.date).format('ll')}</button>
                                    </div>
                                })}
                            </div>
                        </div>
                    </div>
                }
            })}

            
        </div>
    )
}

export default HostReservations;