import React, { useState, useEffect } from 'react'
import moment from 'moment'
import ReactModal from 'react-modal';
import { withRouter } from 'react-router-dom'


const UserReservations = (props) => {
    const [spaces, setSpaces] = useState([])
    const [showModal, setModal] = useState(false)
    const [modalInfo, setModalInfo] = useState({})

    useEffect(() => {
        setSpaces([...props.spaces])
    }, [props.spaces])


    // console.log('spaces', spaces)

    var resArr = []
    spaces.forEach((item, idx) => {
        item.events.filter((ele, idx2) => {
            if (ele.userEmail === props.user.email) {
                // console.log('ele in filter', ele)
                var temp = ele
                temp.space = item
                resArr.push(temp)
            }
        })
    })

    // console.log('resArr', resArr)
    // console.log('modalInfo', modalInfo)

    const handleModal = () => {
        setModal(!showModal);
    }

    return (
        <div className='userPortalReservationSection'>
            <ReactModal
                isOpen={showModal}
                className="userModal"
                overlayClassName="Overlay"
                ariaHideApp={false}
                onRequestClose={handleModal}
                contentLabel="Minimal Modal Example">
                <h4>Reservation {modalInfo.idx + 1}</h4>
                <h5>Date: {moment(modalInfo.date).format('LL')}</h5>
                <h5>Address: {modalInfo.address}</h5>
                <h5>City: {modalInfo.city}</h5>
                <h5>Price: {modalInfo.price}€</h5>
                <h5>Host: {modalInfo.host}</h5>
                <h5>Host email: {modalInfo.hostEmail}</h5>
                <h5>Invoice #: {modalInfo.invoiceNum}</h5>
                <button id='moreInfo' onClick={() => props.history.push(`/singlespace/${modalInfo._id}`)}>Sala Page</button>
                <button onClick={handleModal}>Close</button>
            </ReactModal>

            {resArr.length > 0 ?
                <>
                    <h3>Your reservations</h3>
                    <div className='userReservations'>
                        {resArr.map((item, idx) => {
                            return <div className='reservationModals' key={idx}>
                                <button onClick={() => { return handleModal(), setModalInfo({ idx: idx, _id: item.space._id, address: item.space.locationAddress, city: item.space.locationCity, price: item.space.price, host: item.space.host, hostEmail: item.space.email, date: item.date, invoiceNum: item.invoiceNum }) }}>{moment(item.date).format('ll')}</button>
                            </div>
                        })}
                    </div>
                </>
                : null}
        </div>
    )
}

export default withRouter(UserReservations);