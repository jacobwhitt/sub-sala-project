import React from 'react'

const ReservationPage = (props) => {
    console.log('props reservation page', props)

    return (
        <>
            <h2>You are about to reserve {props.location.state.host}'s space</h2>
            <div>
                <div>
                    <p>Host: {props.location.state.host}</p>
                    <p>Address: {props.location.state.locationAddress}</p>
                    <p>City: {props.location.state.locationCity}</p>
                    <p>Space Description: {props.location.state.description}</p>
                    <p>Date: {props.location.date}</p>
                </div>


                <button onClick={() => props.createCheckoutSession()}>Confirm & Pay</button>
                <p>You are not being charged yet</p>

            </div>
        </>
    )
}


export default ReservationPage