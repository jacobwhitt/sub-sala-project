import React, { useState } from 'react'
import Axios from 'axios'
import { URL } from '../config'

const UserAccount = (props) => {
    console.log('props in userAcct', props.location.state)
    const user = props.location.state.user;

    const [form, setValues] = useState({
        name: '',
        email: '',
        newEmail: '',
        currentpassword: '',
        newpassword: '',
        confirmpassword: ''
    })

    const [message, setMessage] = useState('')
    const handleChange = e => {
        // debugger
        setValues({ ...form, [e.target.name]: e.target.value })
    }

    let token = JSON.parse(localStorage.getItem('token'))
    Axios.defaults.headers.common['Authorization'] = token

    const handleSubmit = async (e) => {
        e.preventDefault()

        try {
            const response = await Axios.put(`${URL}/users/update`, {
                name: form.name,
                email: user.email,
                newEmail: form.newEmail,
                currentpassword: form.currentpassword,
                newpassword: form.newpassword,
                confirmpassword: form.confirmpassword
            })
            setMessage(response.data.message)
            if (response.data.ok) {
                setTimeout(() => {
                    alert('Account info successfully updated!')
                    !props.isHost ? props.history.push({ pathname: '/user' }) : props.history.push({ pathname: '/host' })
                }, 2000)
            }
        }
        catch (error) {
            console.log(error)
        }
    }


    return (<div className='userAcctInfo'>
        <h3 id='editAcctTitle' >Account Info for {user.name}</h3>
        <div className='message'><h4>{message}</h4></div>

        <div>
            <button className='backButton' onClick={() => { user.host || user.admin ? props.history.push('/host') : props.history.push('/user') }}>Back</button>
        </div>

        <form className='editAcctInfo' onSubmit={handleSubmit}
            onChange={handleChange}>

            <div className='userAcctPass'>

                <label htmlFor='name'>Name</label>
                <input id='name' type='input' name='name' defaultValue={user.name} />

                <label>Email</label>
                <p>{user.email}</p>

                <label htmlFor='newEmail'>New Email</label>
                <input id='newEmail' type='text' name='newEmail' />

                <label htmlFor='currentpassword'>Current Password</label>
                <input id='currentpassword' type='password' name='currentpassword' />

                <label htmlFor='newpassword'>New Password</label>
                <input id='newpassword' type='password' name='newpassword' />

                <label htmlFor='confirmpassword'>Confirm Password</label>
                <input id='confirmpassword' type='password' name='confirmpassword' />
            </div>

            <div className='acctInfoSubmit'>
                <button>Submit</button>
            </div>
        </form>

    </div>)

}

export default UserAccount

// , state: { user } 