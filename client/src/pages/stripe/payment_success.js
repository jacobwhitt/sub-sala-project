import React, { useEffect, useState } from "react";
import Axios from "axios";
import { URL } from '../../config'
import moment from 'moment'
import { withRouter, Redirect, Route } from "react-router-dom";

const PaymentSuccess = props => {
  console.log('props in paysucc', props)
  const { history } = props


  useEffect(() => {
    getSessionData();
    setTimeout(() => { return history !== undefined ? history.push('/home') : null; }, 5000);
  }, []);


  const getSessionData = async () => {
    try {
      const reservationInfo = JSON.parse(localStorage.getItem("reservationInfo"));
      console.log('reservationInfo in paySucc', reservationInfo)

      const response = await Axios.post(
        `${URL}/payment/checkout-session?sessionId=${reservationInfo.sessionId}`,
        { date: moment(reservationInfo.date).format('YYYY-MM-DD'), spaceId: reservationInfo.spaceId },
      );
      console.log("res.data in paySucc ==>", response.data);


      const response2 = await Axios.post(`${URL}/emails/confirmation_email/`, { email: response.data.customer.email, invoiceNum: response.data.customer.invoice_prefix, reservationInfo })
      // localStorage.removeItem("reservationInfo");${reservationInfo}

      console.log("== res2.data ==>", response2.data);

    } catch (error) {
      console.log("payment success error in getSessionData:", error)

    }
  };



  return (
    <div className="message_container">
      <div className="message_box">

        <div className="message_box_left">
          <h2>Payment Successful!</h2>
          <h4>Your reservation info was saved in your account page.</h4>
          <h4>You will be redirected shortly.</h4>
        </div>

        {/* <div>
          <button className='backButton' onClick={() => { props.isHost ? props.history.push('/host') : props.history.push('/user') }}>Click here to go to the user portal</button>
        </div> */}

      </div>
    </div>
  );
};

export default withRouter(PaymentSuccess);
