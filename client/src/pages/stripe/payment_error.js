import React from "react";

const PaymentError = props => {
  return (
    <div className="message_container">
      <div style={{ border: "2px solid  #FF395B" }} className="message_box">
        <div className="message_box_left">
          <h3>It seems there's been a mistake...</h3>
          <p>Try going back and trying again. Make sure the email used in your account matches that in the Stripe payment as well!</p>
        </div>
        <div style={{ color: "#FF395B" }} className="message_box_right">
          Payment Error
        </div>
      </div>
    </div>
  );
};

export default PaymentError;
