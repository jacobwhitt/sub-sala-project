import React from "react";
import axios from "axios";
import moment from 'moment';
import { injectStripe } from "react-stripe-elements";
import { URL } from '../../config'


const Checkout = props => {
  console.log('props in checkout', props)
  console.log('space info in checkout', props.location.state)
  console.log('props.history in checkout', props.history)

  const products = [
    {
      name: props.location.state.space.locationAddress,
      images: [props.location.state.space.fotos[0].original],
      quantity: 1,
      amount: props.location.state.space.price // Keep the amount on the server to prevent customers from manipulating on client
    }
  ];
  // console.log('checkout props', props)
  //=====================================================================================
  //=======================  CALCULATE TOTAL FUNCTION  ==================================
  //=====================================================================================
  const calculate_total = () => {
    let total = 0;
    products.forEach(ele => (total += ele.quantity * ele.amount));
    return total;
  };
  //=====================================================================================
  //=======================  CREATE CHECKOUT SESSION  ===================================
  //=====================================================================================

  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        `${URL}/payment/create-checkout-session`,
        { products }
      );
      return response.data.ok
        ? (
          localStorage.setItem("reservationInfo", JSON.stringify({ sessionId: response.data.sessionId, spaceId: props.location.state.spaceId, date: moment(props.location.state.date).format('YYYY-MM-DD') })),
          redirect(response.data.sessionId)
        )
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };


  //!=======================  REDIRECT TO STRIPE CHECKOUT  ========
  const redirect = sessionId => {

    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  //=====================================================================================
  //=====================================================================================
  //=====================================================================================
  return (<>
    <div className="checkoutHeader">Check details before paying</div>
    <div>
      <button className='backButton' onClick={() => { props.history.push(`/singlespace/${props.location.state.spaceId}`) }}>Back</button>
    </div>
    <div className="checkout_container">

      <div className="products_list">
        <h3>You are about to reserve {props.location.state.space.host}'s space</h3>
        <div>
          <p>Host: {props.location.state.space.host}</p>
          <p>Address: {props.location.state.space.locationAddress}</p>
          <p>City: {props.location.state.space.locationCity}</p>
          <p>Date: {moment(props.location.state.date).format('LL')}</p>
          <p>Space Description: {props.location.state.space.description}</p>
          <p>Price: {props.location.state.space.price}</p>
        </div>


      </div>
      <div className="checkoutFooter">
        <div className="total">Total : {calculate_total()} €</div>
        <button className="button" onClick={() => createCheckoutSession()}>
          PAY
        </button>
      </div>
    </div>
  </>);
};

export default injectStripe(Checkout);


