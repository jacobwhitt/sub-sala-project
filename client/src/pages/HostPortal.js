import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'
import ImageGallery from 'react-image-gallery';

const Host = (props) => {
    const [spaces, setSpaces] = useState([])
    const [message, setMessage] = useState('')
    const [user, setUser] = useState([])
    // console.log('user from res.data in hostportal', user)

    const token = JSON.parse(localStorage.getItem('token'))
    useEffect(() => {
        const find_user = async () => {
            try {
                Axios.defaults.headers.common['Authorization'] = token
                const response = await Axios.get(`${URL}/users/findone`);
                // setUser(response.data)
                setUser(response.data)
            }
            catch (error) {
                console.log(error);
            }
        }
        find_user();

        const findBy = async () => {
            let hostID = props.location.state ? props.location.state.owner : undefined;
            try {
                Axios.defaults.headers.common['Authorization'] = token
                const response = await Axios.post(`${URL}/spaces/findby`, { _id: hostID });
                response.data.photos.map((item, idx) => {
                    item.original = `${URL}/assets/${item.filename}`
                    item.thumbnail = `${URL}/assets/${item.filename}`
                })
                console.log('res.data.photos', response.data.photos)

                const temp = []
                response.data.spaces.forEach(space => {
                    temp.push({
                        ...space,
                        photos: response.data.photos.filter(ele => ele.typeId === space._id)
                    })
                })
                console.log('temp in findBy after setting photos =>', temp)
                setSpaces([...temp])
            }
            catch (error) {
                console.log(error);
            }
        }
        findBy();
    }, [token]);

    const handleDelete = async (_id, idx) => {
        try {
            Axios.defaults.headers.common['Authorization'] = token
            const response = await Axios.delete(`${URL}/spaces/delete/${_id}`);
            setMessage(response.data.message)
            let temp = spaces
            temp.splice(idx, 1)
            setSpaces([...temp])
        }
        catch (error) {
            console.log(error);
        }
    }

    const handleUpdate = async (_id, idx) => {
        try {
            if (!props.isAdmin) {
                props.history.push({ pathname: '/UpdateSpaceForm', state: { ...spaces[idx] } })
            } else if (props.isAdmin) {
                props.history.push({ pathname: '/UpdateSpaceForm', state: { ...spaces[idx] } })
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    const addFotos = async (_id, idx) => {
        try {
            props.history.push({ pathname: `/imageuploader/${_id}` })
        }
        catch (error) {
            console.log(error);
        }
    }

    var typemodel = ['meal', 'art', 'music']
    var timemodel = ['morning', 'afternoon', 'night']

    var order = (arg1, arg2) => {
        // console.log(arg1, arg2)
        var temp = ''
        arg2.forEach(item => arg1.includes(item) ? temp += (item + ' ') : null)
        return temp
    }

    // console.log('props.location.state is userID', props.location.state)
    // console.log('this `owner: id` needs to be sent to the list-space and edit-account buttons somehow...')

    //TODO the state being sent to from the adminPortal via "Modify Spaces" button is the ID of the Host that owns the space. Find a way to feed that ID into the "List Your Space" and "Edit Account Info" buttons so they think that admin is the Host and editing the host's info, not the admin's info

    const photos = []
    var screenWidth = window.innerWidth
    // console.log(screenWidth)

    return (<>
        <h3 id='hostportal'>Host Portal</h3>
        <div className='hostActionButtons'>
            <button id='listSpaceButton' onClick={() => { props.history.push('/SpaceCreateForm') }}>List Your Space</button>

            <button id='editAcctInfo' onClick={() => props.history.push({ pathname: '/useraccount', state: { user } })}>Edit Account Info</button>

            <button id='hostReservations' onClick={() => props.history.push({ pathname: '/hostreservations', state: { spaces, user } })}>Reservations</button>
        </div>

        <div className='message'><h4>{message}</h4></div>

        <div className='hostSpaces'>
            {spaces.map((item, idx) => {
                console.log('item in space mapping', item)

                return <div key={idx} className='hostSpaceContainer'>
                    {item.photos.length >= 1 ? <div className="slideshow-container">
                        <ImageGallery
                            showFullscreenButton={false}
                            showPlayButton={false}
                            items={item.photos} />
                    </div> : null}

                    <div className='singleSpace'>
                        <p>Host: {item.host}</p>
                        <p>Address: {item.locationAddress}</p>
                        <p>City: {item.locationCity}</p>
                        <p>Country: {item.locationCountry}</p>
                        <p>Capacity: {item.capacity}</p>
                        <p>Types of Events: {order(item.type, typemodel)}</p>
                        <p>Times of Day: {order(item.timeOfDay, timemodel)}</p>
                        <p>Space Description: {item.description}</p>
                    </div>

                    <div className='editSpaceButtons'>
                        <button onClick={() => props.history.push({ pathname: `/imageuploader/${item._id}/space` }, { state: item._id })}>Edit Photos</button>

                        <button onClick={() => handleUpdate(item._id, idx)}>Update Space</button>

                        <button onClick={() => handleDelete(item._id, idx)}>Delete Space</button>
                    </div>
                </div>
            })}
        </div>

    </>)

}

export default Host



