import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'


const AdminPortal = (props) => {
	const [spaces, setSpaces] = useState([])
	const [user, setUser] = useState([])
	const [message, setMessage] = useState('')

	const token = JSON.parse(localStorage.getItem('token'))

	useEffect(() => {
		const find_spaces = async () => {
			try {
				const response = await Axios.get(`${URL}/spaces/findall`);
				console.log('App.js FIND SPACES', response.data)
				setSpaces([...response.data.spaces])
			}
			catch (error) {
				console.log(error);
			}
		}
		find_spaces();

		const find_users = async () => {
			try {
				Axios.defaults.headers.common['Authorization'] = token
				const response = await Axios.get(`${URL}/users/findall`);
				// setUser(response.data)
				setUser([...response.data])
			}
			catch (error) {
				console.log(error);
			}
		}
		find_users();
	}, [token]);



	
	const handleDelete = async (_id, idx) => {
		try {
			Axios.defaults.headers.common['Authorization'] = token
			const response = await Axios.delete(`${URL}/users/delete/${_id}`);
			setMessage(response.data.message)
			let temp = user
			temp.splice(idx, 1)
			setUser([...temp])
		} catch (error) {
			console.log(error);
		}
	}


	return <div className='adminportal'>
		<h1>Admin Portal</h1>
		<div className='divider'></div>

		{/*//* MAP THE HOSTS TO THEIR OWN SECTION   */}
		<h1>Hosts</h1>
		<div className='adminHosts'>
			
			{user.map((item, idx) => {
				if (item.admin === false && item.host === true) {
					console.log('item=host in admin portal',item)
					return <div key={idx} className='userInfoBoxes'>
						<p>Name: {item.name}</p>
						<p>Email: {item.email}</p>
						<button id='adminUserButtons' onClick={() => props.history.push({ pathname: '/host', state: { owner: item._id } })}>Modify Spaces</button>
						<button id='adminUserButtons' onClick={() => props.history.push({ pathname: '/useraccount', state: { user: item } })}>Edit User Info</button>
						<button id='adminUserButtons' onClick={() => handleDelete(item._id, idx)}>Delete User</button>
					</div>
				}
			})}
		</div>

		{/*//? MAP THE USERS TO THEIR OWN SECTION   */}
		<h1>Users</h1>
		<div className='adminUsers'>
			{user.map((item, idx) => {
				if (item.admin === false && item.host === false) {
					return <div key={idx} className='userInfoBoxes'>
						
						<p>Name: {item.name}</p>
						<p>Email: {item.email}</p>
						<button id='adminUserButtons' onClick={() => props.history.push({ pathname: '/useraccount', state: { user: item } })}>Edit User Info</button>
						<button id='adminUserButtons' onClick={() => handleDelete(item._id, idx)}>Delete User</button>
					</div>
				}
			})}
		</div>

	</div>


}

export default AdminPortal










