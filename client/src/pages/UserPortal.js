import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'
import UserReservations from './Reservations/UserReservations';

const User = (props) => {
    const [user, setUser] = useState([])
    const [spaces, setSpaces] = useState([])
    // console.log('user from res.data in userportal', user)
    console.log('spaces in userportal', spaces)

    const token = JSON.parse(localStorage.getItem('token'))

    useEffect(() => {
        const find_user = async () => {
            try {
                Axios.defaults.headers.common['Authorization'] = token
                const response = await Axios.get(`${URL}/users/findone`);
                setUser(response.data)
            }
            catch (error) {
                console.log(error);
            }
        }
        //? refresh the spaces from the server so any reservations made will now appear
        const find_spaces = async () => {
            try {
                const response = await Axios.get(`${URL}/spaces/findall`);
                // console.log('App.js FIND SPACES', response.data)
                setSpaces([...response.data.spaces])
            }
            catch (error) {
                console.log(error);
            }
        }
        find_user();
        find_spaces();
    }, [token]);


    return (
        <div className='userPortalContainer'>
            <h3>User Portal for {user.name}</h3>

            <div className='userInfo'>
                <div>
                    <p>Name: {user.name}</p>
                    <p>Email: {user.email}</p>
                </div>

                <div>
                    <button className='userEditInfoButton' onClick={() => props.history.push({ pathname: '/useraccount', state: { user } })}>Edit Account Info</button>
                </div>
            </div>

            {
                !user.admin && !user.host ?
                    <div>
                        <UserReservations user={user} spaces={spaces} />
                    </div>
                    : null
            }


        </div>
    )

}

export default User