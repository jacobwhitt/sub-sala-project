import React, { useState } from 'react'
import Axios from 'axios'
import { URL } from '../config'

const Register = (props) => {
	const [form, setValues] = useState({
		name: '',
		email: '',
		password: '',
		password2: ''
	})
	const [message, setMessage] = useState('')
	const handleChange = e => {
		// debugger
		setValues({ ...form, [e.target.name]: e.target.value })
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			//* POST = (PATH, REQ.BODY)
			const response = await Axios.post(`${URL}/users/register`, {
				name: form.name,
				email: form.email,
				password: form.password,
				password2: form.password2,
				host: form.host === 'on' ? true : false
			})
			setMessage(response.data.message)
			if (response.data.ok) {
				setTimeout(() => {
					props.history.push('/home')
				}, 2000)
			}
		}
		catch (error) {
			console.log(error)
		}
	}


	return <>
		<form onSubmit={handleSubmit}
			onChange={handleChange}
			className='form_container'>
			<h3>Registration is easy!</h3>
			<label>Name</label>
			<input name="name" />

			<label>Email</label>
			<input name="email" />

			<label>Password</label>
			<input name="password" />

			<label>Repeat password</label>
			<input name="password2" />

			<label className='hostCheckbox'>
				<input id='hostinput' type='checkbox' name='host' />
				I want to be a host
			</label>

			<button className='registerbutton'>Register</button>
			<div className='message'><h4>{message}</h4></div>
		</form>
	</>
}

export default Register