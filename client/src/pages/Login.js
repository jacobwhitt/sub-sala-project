import React, { useState } from 'react'
import Axios from 'axios'
import { URL } from '../config'


const Login = (props) => {

	const [form, setValues] = useState({
		email: '',
		password: ''
	})

	const [message, setMessage] = useState('')
	const handleChange = e => {
		setValues({ ...form, [e.target.name]: e.target.value })
	}

	const handleSubmit = async (e) => {
		e.preventDefault()
		try {
			const response = await Axios.post(`${URL}/users/login`, {
				email: form.email,
				password: form.password
			})
			setMessage(response.data.message)
			if (response.data.ok) {
				setTimeout(() => {
					console.log('in login', response.data)
					let { token, admin, host, email } = response.data
					props.login(token, admin, host, email)
					console.log(admin, host)
					if (admin) {
						return props.history.push('/adminportal')
					}
					
					if (host) {
						return props.history.push('/host')
					}
					return props.history.push('/home')
				}, 1000)
			}
		}
		catch (error) {
			console.log(error)
		}
	}

	return <form onSubmit={handleSubmit}
		onChange={handleChange}
		className='form_container'>
		<h3>Login to get started!</h3>
		<label>Email</label>
		<input name="email" />
		<label>Password</label>
		<input name="password" type='password' />
		<button className='loginbutton'>Login</button>
		<div className='message'><h4>{message}</h4></div>
	</form>
}

export default Login










