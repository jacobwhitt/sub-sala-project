import React, { useState } from 'react'
import Axios from 'axios'
import { URL } from '../config'


const SpaceCreateForm = (props) => {
    console.log('from SpaceCreate:', props)
    const [newSpace, setSpaceValues] = useState({
        locationAddress: '',
        locationCity: '',
        locationCountry: '',
        capacity: '',
        description: '',
        price: '',
        type: [],
        timeOfDay: []
    })


    const [message, setMessage] = useState('')


    const handleChange = e => {
        setSpaceValues({ ...newSpace, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (newSpace.type.length === 0 || newSpace.timeOfDay.length === 0) return alert('Please fill out all forms and check-boxes :)')

        try {
            const response = await Axios.post(`${URL}/spaces/create`,
                {
                    locationAddress: newSpace.locationAddress,
                    locationCity: newSpace.locationCity,
                    locationCountry: newSpace.locationCountry,
                    capacity: Number(newSpace.capacity),
                    description: newSpace.description,
                    price: Number(newSpace.price),
                    type: newSpace.type,
                    timeOfDay: newSpace.timeOfDay
                });
            window.scrollTo(0, 0)
            setMessage(response.data.message);
            if (response.data.ok) {
                setSpaceValues({
                    locationAddress: '',
                    locationCity: '',
                    locationCountry: '',
                    capacity: '',
                    description: '',
                    price: '',
                    type: [],
                    timeOfDay: []
                });
                document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);
                setTimeout(() => {
                    props.history.push('/host');
                }, 2000);
            }
            //console.log(response)
        }
        catch (error) {
            console.log(error);
        }
    }


    //* EVENT HANDLERS FOR THE TYPE OF EVENTS CHECK BOXES
    const typeHandler = (e) => {
        console.log('event:', e.target.value)
        let tempType = newSpace.type
        let index = tempType.findIndex(ele => ele === e.target.value)

        if (index === -1) {
            tempType.push(e.target.value)
        } else {
            tempType.splice(index, 1)
        }
        setSpaceValues({ ...newSpace, type: [...tempType] })
        console.log('newSpace.type', newSpace.type)
    }

    //? EVENT HANDLERS FOR THE TIME OF DAY CHECK BOXES
    const timeHandler = (e) => {
        console.log('event:', e.target.value)
        let tempTime = newSpace.timeOfDay
        let index = tempTime.findIndex(ele => ele === e.target.value)

        if (index === -1) {
            tempTime.push(e.target.value)
        } else {
            tempTime.splice(index, 1)
        }
        setSpaceValues({ ...newSpace, timeOfDay: [...tempTime] })
        console.log('newSpace.timeOfDay', newSpace.timeOfDay)
    }

    // //? HANDLE CALENDAR DATA
    // const dateHandler = (startDate, endDate) => {
    //     //? pass info to props.setDates
    //     console.log('DateHandler in HOME', startDate + ' - ' + endDate)
    //     setSpaceValues({ newSpace.events: }
    // }

    return (<>
        <h1 className='spaceCreateTitle'>Space Info</h1>
        <h4 className='message'>{message}</h4>
        <div>
            <button className='backButton' onClick={() => { props.history.push('/host') }}>Back</button>
        </div>

        <form className='spaceCreateForm' onSubmit={handleSubmit}>
            <div className='spaceformleft'>
                <label>Address</label>
                <input required name='locationAddress' type='text' onChange={handleChange} value={newSpace.locationAddress} ></input>

                <label>City</label>
                <input required name='locationCity' type='text' onChange={handleChange} value={newSpace.locationCity}></input>

                <label>Country</label>
                <input required name='locationCountry' type='text' onChange={handleChange} value={newSpace.locationCountry}></input>

                <label>Capacity</label>
                <input required name='capacity' type='number' onChange={handleChange} value={newSpace.capacity}></input>

                <label>Price</label>
                <input required name='price' type='number' min='50' onChange={handleChange} value={newSpace.price}></input>

                <label>Description</label>
                <textarea className='spaceDescription' required name='description' type='text' onChange={handleChange} value={newSpace.description}></textarea>
            </div>

            <div className='spaceformright'>
                <label>Types of Events</label>
                <div id='type' className='eventCreateType'>

                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={typeHandler} value='meal' />
                            <p>Host meals</p>
                        </label>
                    </div>
                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={typeHandler} value='art' />
                            <p>Host art showcases</p>
                        </label>
                    </div>
                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={typeHandler} value='music' />
                            <p>Host music events</p>
                        </label>
                    </div>

                </div>

                <label>Times of Day</label>
                <div id="timeOfDay" name="timeOfDay">
                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={timeHandler} value='morning' />
                            <p>Mornings</p>
                        </label>
                    </div>
                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={timeHandler} value='afternoon' />
                            <p>Afternoons</p>
                        </label>
                    </div>
                    <div>
                        <label className='spaceCreateCheckbox'>
                            <input id='hostinput' type="checkbox" onChange={timeHandler} value='night' />
                            <p>Nights</p>
                        </label>
                    </div>
                </div>

                <button onClick={() => props.history.push('/SpaceCreateForm')}>Submit</button>
            </div>
        </form>
    </>)

}

export default SpaceCreateForm