import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'
import CalendarReservations from "../components/CalendarReservations"
import ImageGallery from 'react-image-gallery';

const Space = (props) => {
    console.log('props in SingleSpace', props)
    console.log('a change to test deployment')

    const [space, setSpace] = useState({ type: [], timeOfDay: [], fotos: [], events: [] })
    const [date, setDate] = useState('')
    var spaceId = props.match.params._id
    console.log('spaceId in Space', spaceId)

    //? GET SPACE FROM SERVER
    useEffect(() => {
        const getSpace = async () => {
            try {
                const response = await Axios.get(`${URL}/spaces/findone/${spaceId}`);

                response.data.photos.map((item, idx) => {
                    item.original = `${URL}/assets/${item.filename}`
                    item.thumbnail = `${URL}/assets/${item.filename}`
                })

                // console.log('response.data in Space', response.data)
                setSpace({
                    host: response.data.space.host,
                    email: response.data.space.email,
                    fotos: [...response.data.photos],
                    events: [...response.data.space.events],
                    locationAddress: response.data.space.locationAddress,
                    locationCity: response.data.space.locationCity,
                    locationCountry: response.data.space.locationCountry,
                    timeOfDay: [...response.data.space.timeOfDay],
                    type: [...response.data.space.type],
                    capacity: response.data.space.capacity,
                    price: response.data.space.price,
                    description: response.data.space.description,
                })
            }
            catch (error) {
                console.log(error);
            }
        }
        getSpace();
    }, []);


    //? HANDLE CALENDAR DATA
    const dateHandler = (e) => {
        //? set date chosen in the state
        console.log('dateHandler ==> date', e._d)
        setDate(e._d)
    }

    useEffect(() => {
        console.log('post-handled/set date', date)
    }, [date])


    return (
        <>
            <p>Welcome to {space.host}'s space</p>
            <div>
                <button className='backButton' onClick={() => { props.history.push('/search') }}>Back</button>
            </div>
            <div className='singleSpaceContainer'>

                <div className='singleSpaceImages'>
                    <div className="slideshow-container">
                        <ImageGallery
                            showFullscreenButton={false}
                            showPlayButton={false}
                            items={space.fotos} />
                    </div>
                </div>

                <div className='singleSpace'>
                    <p>Host: {space.host}</p>
                    <p>Address: {space.locationAddress}</p>
                    <p>City: {space.locationCity}</p>
                    <p>Country: {space.locationCountry}</p>
                    <p>Capacity: {space.capacity}</p>
                    <p>Types of Events: {props.order(space.type, props.typeArr)}</p>
                    <p>Times of Day: {props.order(space.timeOfDay, props.timeArr)}</p>
                    <p>Space Description: {space.description}</p>
                </div>

                <p>—————————————</p>
                <h5 style={{ color: 'red' }}>Please confirm the date you would like from this space's availability:</h5>
                <CalendarReservations {...space} date={props.date} dateHandler={dateHandler} />

                <button className='reserveButton' onClick={() => {
                    !window.localStorage.token ? alert(`Please be sure to register before booking salas!`)
                        : window.localStorage.token && date ? props.history.push({ pathname: '/stripe', state: { space, date, spaceId } }) : alert(`Please be sure to confirm the date below!`)
                }}>Reserve Space</button>
            </div>
        </>
    )
}

export default Space;