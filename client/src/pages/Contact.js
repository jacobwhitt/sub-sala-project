import React, { useState } from 'react'
import Axios from 'axios'
import { URL } from '../config'

const Contact = (props) => {
    const [form, setForm] = useState({
        name: '',
        email: '',
        subject: '',
        message: ''
    })
    const [message, setMessage] = useState('')


    const handleChange = (e) => {
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await Axios.post(`${URL}/emails/send_email`, {
                name: form.name,
                email: form.email,
                subject: form.subject,
                message: form.message,
            })
            // setMessage(response.data.message)
            if (response.ok) {
                setMessage('Email sent!')
            }
            console.log('res.data in contact.js', response.data)
            if (response.data.ok) {
                alert('Email sent!')
                setTimeout(() => {
                    props.history.push('/home')
                }, 2000)
            }
        }
        catch (error) {
            console.log(error)
        }
    }


    return (<div>
        <form className='contactForm' onChange={handleChange} onSubmit={handleSubmit}>

            <h3 className='contactTitle'>Contact Us!</h3>

            <input id='contact' name='name' placeholder='Name' type='text'></input>

            <input id='contact' name='email' placeholder='Email' type='email'></input>

            <input id='subject' name='subject' placeholder='Subject' type='text'></input>

            <textarea id='message' name='message' placeholder='Type your message here' type='text'></textarea>

            <button>Send away!</button>
            {/* <h4>{message}</h4> */}
        </form>
        <h3>{message}</h3>
    </div>)

}

export default Contact