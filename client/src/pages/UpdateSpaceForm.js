import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'

const UpdateSpaceForm = (props) => {
    const token = JSON.parse(localStorage.getItem('token'))
    const [message, setMessage] = useState('')
    const [space, setSpace] = useState(props.location.state)
    const [newSpace, setNewSpace] = useState({
        locationAddress: '',
        locationCity: '',
        locationCountry: '',
        capacity: '',
        description: '',
        price: '',
        // events: []
    })
    const [type, setType] = useState([])
    const [timeOfDay, setTimeOfDay] = useState([])


    //! SET THE CHECKBOXES FROM THE STATE, UPDATE NEWSTATE FOR OVERWRITING STATE
    const [typemodel, setTypeModel] = useState([
        { name: 'meal', checked: false, display: 'Host meals' },
        { name: 'art', checked: false, display: 'Host art showcases' },
        { name: 'music', checked: false, display: 'Host music events' }])
    const [timemodel, setTimeModel] = useState([
        { name: 'morning', checked: false, display: 'Mornings' },
        { name: 'afternoon', checked: false, display: 'Afternoons' },
        { name: 'night', checked: false, display: 'Nights' }
    ])

    //! CHECKBOX FUNCTION TO COMPARE STATE TO MODEL, TURN ON/OFF ACCORDINGLY
    var order = (model, arr, fun1, fun2) => {
        let temp = model
        temp.forEach(item => arr.includes(item.name) ? item.checked = true : item.checked = false);
        fun1([...temp]);
        fun2([...arr])
    }

    //! CHECKBOX FUNCTIONS DISPLAY ON/OFF IN ORDER IN SPACE'S INFO
    useEffect(() => {
        order(typemodel, props.location.state.type, setTypeModel, setType)
        order(timemodel, props.location.state.timeOfDay, setTimeModel, setTimeOfDay)
    }, [])
    // useEffect(() => {
    //     order(typemodel, props.typeArr, setType)
    //     order(timemodel, props.timeArr, setTimeOfDay)
    // }, [])

    const handleChange = e => {
        setNewSpace({ ...newSpace, [e.target.name]: e.target.value })
        // console.log('setNewSpace:', newSpace)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await Axios.put(`${URL}/spaces/update`,
                {
                    locationAddress: newSpace.locationAddress || space.locationAddress,
                    locationCity: newSpace.locationCity || space.locationCity,
                    locationCountry: newSpace.locationCountry || space.locationCountry,
                    capacity: Number(newSpace.capacity) || Number(space.capacity),
                    description: newSpace.description || space.description,
                    owner: space.owner,
                    _id: space._id,
                    price: Number(newSpace.price) || Number(space.price),
                    type: type.length === 0 ? space.type : type,
                    timeOfDay: timeOfDay.length === 0 ? space.timeOfDay : timeOfDay
                });
            window.scrollTo(0, 0)
            setMessage(response.data.message);
            setTimeout(() => {
                // debugger
                props.isAdmin ? props.history.push({ pathname: '/host', state: { owner: space.owner } }) : props.history.push('/host');
            }, 2000);
            
        }
        catch (error) {
            console.log(error);
        }
    }
    //* EVENT HANDLERS FOR THE TYPE OF EVENTS CHECK BOXES
    const typeHandler = (e) => {
        let tempType = type;
        let index = tempType.findIndex(ele => ele === e.target.value);
        if (index === -1) {
            tempType.push(e.target.value)
        } else {
            tempType.splice(index, 1)
        }
        console.log('tempType', tempType)
        order(typemodel, tempType, setTypeModel, setType)
        //typeorder(typemodel, tempType)
    }
    //? EVENT HANDLERS FOR THE TIME OF DAY CHECK BOXES
    const timeHandler = (e) => {
        let tempTime = timeOfDay
        let index = tempTime.findIndex(ele => ele === e.target.value)
        // console.log('TIME handler index', index)
        if (index === -1) {
            tempTime.push(e.target.value)
        } else {
            tempTime.splice(index, 1)
        }
        console.log('tempTime', tempTime)

        order(timemodel, tempTime, setTimeModel, setTimeOfDay)
        //timeorder(timemodel, tempTime, 'timeOfDay', setTimeModel)
    }
    //? EVENT HANDLER FOR CALENDAR API
    const dateHandler = (e) => {
        console.log(e.target)
    }

    return (<>
        <h1 className='updateSpaceTitle'>Update Space Info</h1>
        <h4 className='message'>{message}</h4>
        <div>
            <button className='backButton' onClick={() => { props.history.push('/host') }}>Back</button>
        </div>

        <form className='spaceCreateForm' onChange={handleChange} onSubmit={handleSubmit}>

            <div className='spaceformleft'>

                <label>Address</label>
                <input name='locationAddress' type='text' placeholder={space.locationAddress} value={newSpace.locationAddress} ></input>

                <label>City</label>
                <input name='locationCity' type='text' placeholder={space.locationCity} value={newSpace.locationCity}></input>

                <label>Country</label>
                <input name='locationCountry' type='text' placeholder={space.locationCountry} value={newSpace.locationCountry}></input>

                <label>Capacity</label>
                <input name='capacity' type='number' placeholder={space.capacity} value={newSpace.capacity}></input>

                <label>Price</label>
                <input name='price' type='number' min='50' placeholder={space.price} value={newSpace.price}></input>

                <label>Description</label>
                <textarea className='spaceDescription' name='description' type='text' placeholder={space.description} value={newSpace.description}></textarea>

            </div>

            <div className='spaceformright'>
                <label>Types of Events</label>
                <div id='type' className='eventCreateType'>

                    <div>
                        <label className={typemodel[0].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={typemodel[0].checked}
                                onChange={typeHandler} value='meal' />
                            <p>Host meals</p>
                        </label>
                    </div>
                    <div>
                        <label className={typemodel[1].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={typemodel[1].checked}
                                onChange={typeHandler} value='art' />
                            <p>Host art showcases</p>
                        </label>
                    </div>
                    <div>
                        <label className={typemodel[2].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={typemodel[2].checked}
                                onChange={typeHandler} value='music' />
                            <p>Host music events</p>
                        </label>
                    </div>

                </div>

                <label>Times of Day</label>
                <div id="timeOfDay" name="timeOfDay">
                    <div>
                        <label className={timemodel[0].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={timemodel[0].checked}
                                onChange={timeHandler} value='morning' />
                            <p>Mornings</p>
                        </label>
                    </div>
                    <div>
                        <label className={timemodel[1].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={timemodel[1].checked}
                                onChange={timeHandler} value='afternoon' />
                            <p>Afternoons</p>
                        </label>
                    </div>
                    <div>
                        <label className={timemodel[2].checked ? 'spaceCreateCheckbox-Checked' : 'spaceCreateCheckbox'}>
                            <input id='hostinput' type="checkbox"
                                checked={timemodel[2].checked}
                                onChange={timeHandler} value='night' />
                            <p>Nights</p>
                        </label>
                    </div>
                </div>

                <button className='updateSpaceFormSubmit' onClick={() => props.history.push('/UpdateSpaceForm')}>Submit</button>
            </div>
        </form>
    </>)
}
export default UpdateSpaceForm