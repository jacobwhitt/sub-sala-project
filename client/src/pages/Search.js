import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { URL } from '../config'
import ImageGallery from 'react-image-gallery';
import CalendarSearch from "../components/CalendarSearch"
import moment from 'moment'

const SearchResults = (props) => {
    console.log('Search.js props', props)

    const typemodel = ['meal', 'art', 'music']
    const timemodel = ['morning', 'afternoon', 'night']
    const [city] = useState(props.city);

    // $(document).ready(function () {
    //     $('[data-toggle="popover"]').popover({
    //         placement: 'top',
    //         trigger: 'hover'
    //     });
    // });

    //! FIND SPACES IN CHOSEN CITY FROM SEARCHBAR
    useEffect(() => {

        // const set_filters = () => {
        //     props.type.forEach((item) => {
        //         document.getElementsByName(`${item}`)[0].checked = true
        //     })
        //     props.time.forEach((item) => {
        //         document.getElementsByName(`${item}`)[0].checked = true
        //     })
        // }
        // set_filters();

        const find_locations = async () => {
            try {
                const response = await Axios.get(`${URL}/spaces/findlocations`);
                // console.log('res.data in Search', response.data)
                props.setLocations([...response.data.locations])
            }
            catch (error) {
                console.log(error);
            }
        }
        find_locations();
    }, []);



    //? USERS CHOOSE FROM CITIES WITH AVAILABLE SPACES PERIOD
    const handleCity = (e) => {
        props.setCity(e.target.value)
        console.log('city in Search', e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        // console.log(props.type, props.time, props.city)
        if (props.type.length === 0 && props.time.length === 0) {
            alert('Please choose at least one:\n • Type of event \n • Time of day')
        } else if (props.city === '') {
            alert('Please select a city')
        }
        else {
            props.displaySearch();
            props.history.push('/search')
        }
    }


    // ? THE SEARCH CONTAINER WILL USE THE SAME CSS FOR ITS SEARCH BAR AS THE HOME CONTAINER
    return <>
        <div className='searchcontainer'>

            <form className='homeSearch' onSubmit={handleSubmit}>

                <div className='switchesGroup'>

                    {/*//* TYPE OF EVENT SWITCHES */}
                    <div className='searchSwitches'>
                        <div>
                            <label>
                                <label className="switch">
                                    <input id='mealswitch' name='meal' type="checkbox"
                                        checked={window.localStorage.getItem('meal') ? true : false}
                                        onChange={props.handleType} /> <span className="slider round"></span>
                                </label> Dining
                                </label>
                        </div>

                        <div>
                            <label>
                                <label className="switch">
                                    <input id='artswitch' name='art' type="checkbox"
                                        checked={window.localStorage.getItem('art') ? true : false}
                                        onChange={props.handleType} />
                                    <span className="slider round"></span>
                                </label> Art Showcase
                                </label>
                        </div>

                        <div>
                            <label>
                                <label className="switch">
                                    <input id='musicswitch' name='music' type="checkbox"
                                        checked={window.localStorage.getItem('music') ? true : false}
                                        onChange={props.handleType} />
                                    <span className="slider round"></span>
                                </label> Music Event
                                </label>
                        </div>
                    </div>

                    {/*//? TIME OF EVENT SWITCHES */}
                    <div className='searchSwitches'>
                        <div>
                            <label>
                                <label className="switch">
                                    <input id='morningswitch' name='morning' type="checkbox"
                                        checked={window.localStorage.getItem('morning') ? true : false}
                                        onClick={props.handleTime} /> <span className="slider round"></span>
                                </label> Morning
                                </label>
                        </div>

                        <div>
                            <label>
                                <label className="switch">
                                    <input id='afternoonswitch' name='afternoon' type="checkbox"
                                        checked={window.localStorage.getItem('afternoon') ? true : false}
                                        onClick={props.handleTime} />
                                    <span className="slider round"></span>
                                </label> Afternoon
                                </label>
                        </div>

                        <div>
                            <label>
                                <label className="switch">
                                    <input id='nightswitch' name='night' type="checkbox"
                                        checked={window.localStorage.getItem('night') ? true : false}
                                        onClick={props.handleTime} />
                                    <span className="slider round"></span>
                                </label> Night
                                </label>
                        </div>
                    </div>

                </div>

                <div className='searchRight'>
                    <CalendarSearch handleDate={props.handleDate}
                        date={props.date} />

                    <label htmlFor="city"></label>
                    <select id='city' defaultValue={props.locations.includes(city) ? city : "Select a city..."} onChange={handleCity}>
                        {props.locations.map((item, idx) => {
                            return <option key={idx} value={item}>{item}</option>
                        })}
                        <option value='city'>Select a city...</option>
                    </select>

                </div>

                <button className='searchButton'>Find a Sala!</button>

            </form>

        </div>


        <div className='searchTitle'>
            <h1>Here are your filtered results</h1>
            <button class='searchInfoModal'>?</button>
        </div>
        
        <div className='searchResultsGrid'>
            {props.filteredSpaces.map((item, idx) => {
                console.log('space `item` in search', item)

                item.events.forEach((item2) => {
                    if (item2.date === props.date) {
                        return null
                    }
                })
                return <div key={idx} className='searchResult'>
                    <div className='singleSpace'>
                        {item.photos.length >= 1 ? <div className="searchImage">
                            <ImageGallery
                                onClick={() => props.history.push(`/singlespace/${item._id}`)}
                                showPlayButton={false}
                                showFullscreenButton={false}
                                showThumbnails={false}
                                items={item.photos} />
                        </div> : null}


                        <div className='searchResultText' onClick={() => {
                            props.getSpace(item._id)
                             props.history.push(`/singlespace/${item._id}`)}}>
                            <p>Capacity: {item.capacity}</p>
                            <p>Price: {item.price}</p>
                            <p>{item.locationAddress}</p>
                        </div>
                    </div>
                </div>
            })}
        </div>
    </>
}

export default SearchResults