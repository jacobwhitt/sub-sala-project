# SubSala

SubSala is an e-commerce site used to find spaces to rent for meeetings, lunches/dinners, art showcases or music events, inspired by Airbnb.
I would eventually like to re-design it and market it specifically to artists and young people vs. tourism and businesspeople in general.
There's an extreme lack of connection between the little people in Barcelona for finding GREAT event spaces, and they definitely exist,
so someone [me] has to try to make these connections happen!

## Getting Started

Git clone the repo here or simply follow this live link: http://159.89.1.248/home
### Prerequisites

```
react-dates = https://github.com/airbnb/react-dates
react-image-gallery = https://github.com/xiaolin/react-image-gallery/blob/master/example/app.js
moment.js = https://momentjs.com/
stripe-js react = https://stripe.com/docs/stripe-js/react
```

### Installing

After git cloning the repo, type the lines below in the respective folders to install dependencies and then launch the page in your browser locally

/client

```
npm install
npm start
```

/server folder

```
npm install
node index.js
```

You will also need a .env file within each of these folders which should look like this and include your own info:

/client/.env

```
REACT_APP_STRIPE_PUBLIC_KEY=[YOUR PUB KEY]
```

/server/.env

```
JWT_SECRET=secret
ATLASURL=mongodb+srv://[YOUR USER NAME]:[YOUR PASSWORD]@cluster0-nznmk.mongodb.net/[YOUR DB NAME]?retryWrites=true&w=majority
NODEMAILER_EMAIL = [EMAIL]
NODEMAILER_PASSWORD = [PASSWORD]

# Stripe API keys
STRIPE_SECRET_KEY=[YOUR STRIPE KEY HERE]

# Required to run webhook
# See README on how to use the Stripe CLI to setup
STRIPE_WEBHOOK_SECRET=whsec_1234
STATIC_DIR='../client'

# Checkout settings
DOMAIN=http://localhost:3000
BASE_PRICE=1
CURRENCY=eur

# Supported payment methods for the store.
# Some payment methods support only a subset of currencies.

PAYMENT_METHODS="card"
# PAYMENT_METHODS="card, ideal" # Requires CURRENCY=eur
```

## Basic Functions

- Register as user/host and login/logout
- List/update/delete a space as a host and upload images to it
- Search for spaces
- Make reservations at spaces
- Stripe secure payment system
- See reservations made/received in the user/host portal
- Email notifications about reservations made/received upon payment success
- Contact the web admin

- In development: ability to cancel reservations as both user or host with email notifications and a minimum time limit before the date.

## Using the site to reserve a space

- One should first register using the NavBar at the top, then you can begin searching for spaces to rent.
- Choose at least one type of event or time of day, or multiple of each if you want to see more results.
- Choose a day that you'd like to reserve a space and a city from the drop menu. Only cities with available spaces will appear in this list.
- Once in the search page, you can redefine your filters and search again or click on one of the space's images to see it's page with more info.
- From the single space page you must confirm the date you wish you reserve below and hit the 'reserve space' button.
- You will be directed to a checkout screen to confirm the details before being redirected to Stripe to "pay" (using 4242 repeating for CC #).
- NOTE: You must use the email you registered with in Stripe to save the details in your account portal correctly.
- Upon completion you should be shown a success/error page and redirected to the home page again.
- You can now check your User Portal for the reservations you've made which appear as modal buttons by date

## Using the site to list a space as a host

- Register a user and choose 'host'
- Go to the host portal and click 'List a space' and fill out all the information
- Upon redirection, upload some images to your new space with the button under the info section
- With each image, hit upload and once you have completed uploading all single images, hit 'submit'
- You can udpate the info of your space at any time with the 'update space' button below
- Reservations at your spaces will appear in the page opened by the 'reservations' button at the top of the page

## Built With

- [React](https://reactjs.org/) - Client-side framework
- [Express](https://expressjs.com/) - Server-side framework
- [Mongo DB Atlas](https://www.mongodb.com/cloud/atlas) - DB management
- [Nodemailer](https://www.npmjs.com/package/nodemailer-react) - Email module
- [Stripe](https://stripe.com/) - Payment system for reservations
- [Airbnb React Dates](https://github.com/airbnb/react-dates) - Calendar API used for search filtering/date blocking/reservations
- [Digital Ocean](https://www.digitalocean.com/) - Deployment

## Author

- **Jacob Whitt** - [Gitlab](https://gitlab.com/jacobwhitt) [Linkedin](https://www.linkedin.com/in/jacob-tw-82305714b/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
