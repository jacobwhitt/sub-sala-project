const User = require('../models/users.models');
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const jwt_secret = process.env.JWT_SECRET

class UserControllers {
    //! (POST) REGISTER
    async register(req, res) {

        const { name, email, password, password2 } = req.body;

        if (!name || !email || !password || !password2) return res.json({ ok: false, message: 'Please fill out all fields' });
        if (password !== password2) return res.json({ ok: false, message: 'Passwords must match' });
        if (!validator.isEmail(email)) return res.json({ ok: false, message: 'Please provide a valid email' })
        try {
            const user = await User.findOne({ email })
            if (user) return res.json({ ok: false, message: 'Email is already in use' });
            const hash = await argon2.hash(password)
            console.log('hash =', hash)
            const newUser = {
                name,
                email,
                password: hash,
                admin: false,
                host: req.body.host
            }
            await User.create(newUser)
            res.json({ ok: true, message: 'Registration successful: Welcome to SubSala!' })
        } catch (error) {
            res.json({ ok: false, error })
        }
    }

    //! (POST) LOGIN
    async login(req, res) {

        const { email, password, admin, host } = req.body;

        if (!email || !password) res.json({ ok: false, message: 'All field are required' });
        if (!validator.isEmail(email)) return res.json({ ok: false, message: 'please provide a valid email' });
        try {

            const user = await User.findOne({ email }).lean();
            if (!user) return res.json({ ok: false, message: 'please provide a valid email' });
            const match = await argon2.verify(user.password, password);
            if (match) {
                //? WHEN WE CREATE A TOKEN FOR THIS USER WE GET THE DATA BACK (registered user info + mongoDB ID) 
                const token = jwt.sign(user, jwt_secret, { expiresIn: 100080 });//{expiresIn:'365d'}
                res.json({ ok: true, message: 'Welcome back!', token, email, admin: user.admin, host: user.host })
            }
            else return res.json({ ok: false, message: 'invalid password' })
        } catch (error) {
            console.log(error)
            res.json({ ok: false, error })
        }
    }

    //! (POST) VERIFY TOKEN/USER ID
    async verify_token(req, res) {

        // console.log(req.headers.authorization)
        const token = req.headers.authorization;

        jwt.verify(token, jwt_secret, (err, succ) => {
            err ? res.json({ ok: false, message: 'something went wrong' }) : res.json({ ok: true, email: succ.email, admin: succ.admin, host: succ.host })
        });
    }

    //! (PUT) UPDATE USER
    async update(req, res) {
        const token = req.headers.authorization;
        const { name, email, newEmail, currentpassword, newpassword, confirmpassword } = req.body;


        if (newpassword !== confirmpassword) return res.json({ ok: false, message: 'passwords must match' });

        try {
            const verify = await jwt.verify(token, jwt_secret)
            if (!verify) {
                return res.send({ ok: false, message: 'invalid token' })
            }
            console.log('verify', verify)

            const user = await User.findOne({ email }).lean();
            if (!user) return res.json({ ok: false, message: 'please provide a valid email' });
            console.log('user', user)

            const match = await argon2.verify(user.password, currentpassword);
            if (match || newEmail || name !== user.name) {
                const hash = await argon2.hash(newpassword)

                const users = await User.updateOne({ _id: verify._id }, { name: name ? name : user.name, email: newEmail ? newEmail : email, password: hash });
                console.log('users in match', users)
                res.send(users);
            }
        } catch (e) {
            console.log('passwords dont match?', e)
            res.send({ e })
        }
    }

    //! (DELETE) DELETE SPACE
    async delete(req, res) {
        let { _id } = req.params;
        console.log(_id)

        try {
            const removed = await User.deleteOne({ _id });
            res.send({ message: 'User removed from database', removed });
        }
        catch (error) {
            console.log(error)
            res.send({ message: 'Error removing user from database', error });
        };
    }

    //! (GET) FIND SINGLE USER
    async findOne(req, res) {
        const token = req.headers.authorization;

        try {
            const verify = await jwt.verify(token, jwt_secret)
            if (!verify) {
                return res.send({ ok: false, message: 'invalid token' })
            }

            const users = await User.findOne({ _id: verify._id });
            res.send(users);
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

    //! (GET) FIND ALL USERS
    async findAll(req, res) {
        const token = req.headers.authorization;

        try {
            const verify = await jwt.verify(token, jwt_secret)
            if (!verify || !verify.admin) {
                return res.send({ ok: false, message: 'invalid token' })
            }

            const users = await User.find();
            res.send(users);
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

}

module.exports = new UserControllers();