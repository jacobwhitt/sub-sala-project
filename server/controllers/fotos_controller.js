const multer = require("multer");
const Images = require("../models/fotos_model");
const Space = require("../models/space_model");
const fs = require("file-system");

const uploadImage = async (req, res, upload, files) => {
  try {
    upload(req, res, async function (err) {
      if (err instanceof multer.MulterError) {
        return res.status(500).json(err);
        // A Multer error occurred when uploading.
      } else if (err) {
        return res.status(500).json(err);
        // An unknown error occurred when uploading.
      }
      // here we can add filename or path to the DB
      console.log(files)


      const tempArr = []

      files.forEach(async (ele) => {
        var createImage = await Images.create({
          pathname: ele.path,
          filename: ele.filename,
          typeId: req.params._id,
          type: req.params.type
        });
        const images = await Space.updateOne({ _id: req.params._id }, { $push: { fotos: createImage._id } });
      })

      return res.status(200).json({ ok: true, message: 'foto uploaded' });
      // Everything went fine.
    });
  } catch (error) {
    console.log("error =====>", error);
  }
};

const fetchSpaceImages = async (req, res) => {
  // console.log('fetchSpaceImages request:', req)
  try {
    const photos = await Images.find({})
    res.status(200).json({ photos });
  } catch (error) {
    console.log("error =====>", error);
  }
};

const fetchImagesForHost = async (req, res) => {
  console.log('fetchSpaceImages request:', req.params)
  try {
    const photos = await Images.find({ typeId: req.params._id })
    res.status(200).json({ photos });
  } catch (error) {
    console.log("error =====>", error);
  }
};

// const fetchFirstImage = async (req, res) => {
//   console.log('fetchFirstImage req:', req)
//   try {
//     const ids = Space.map(space => space._id)
//     console.log(ids)

//     const allImages = await Images.find({ typeId: { $in: ids } })

//     const photo = await Images.findOne({});
//     res.status(200).json({ photo });
//   } catch (error) {
//     console.log("error =>", error);
//   }
// };

const deleteImage = async (req, res) => {
  const { _id, filename } = req.params;
  console.log(_id, filename)
  try {
    //? delete from image collection in Cluster
    const deleted = await Images.deleteOne({ _id });

    //! delete from public folder in server
    fs.unlink(`./public/${filename}`, err => {
      if (err) throw err;
      console.log(`./public/${filename} was deleted`);
      return res.status(200).json({ message: `${filename} was deleted` });
    });

    //* delete from space in client
    const deleteInSpace = await Space.delete({ _id })

    db.stores.update(
      { _id: req.params._id },
      { $pull: { fotos: _id } }
    )



  } catch (error) {
    console.log("error =====>", error);
  }
};

module.exports = {
  uploadImage,
  fetchSpaceImages,
  fetchImagesForHost,
  deleteImage
};
