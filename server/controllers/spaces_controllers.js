const Spaces = require('../models/space_model')
const Images = require('../models/fotos_model')
const jwt = require('jsonwebtoken');
const jwt_secret = process.env.JWT_SECRET

const verify_token = async (token) => {
    let succ = await jwt.verify(token, jwt_secret)
    return succ.host || succ.admin ? succ : false
    console.log('succ:', succ)
    console.log('succ.name:', succ.name)
}

class SpacesControllers {

    //! (POST) ADD SPACE
    async create(req, res) {
        console.log('this is the spaceCreate controller')
        const token = req.headers.authorization;
        const succ = await verify_token(token);
        if (!succ) { return res.json({ ok: false, message: 'You must be a host to create a space, sorry!' }) }

        console.log('req.body in space_controllers/add', req.body)

        const { name, locationAddress, locationCity, locationCountry, capacity, description, type, timeOfDay, price } = req.body;

        if (!locationAddress || !locationCity || !locationCountry || !capacity || !description || !timeOfDay || !type || !price) { return res.json({ ok: false, message: 'Please fill in all fields', required: true }) }

        try {
            const matchSpace = await Spaces.findOne({ locationAddress, locationCity, owner: succ._id });

            if (matchSpace) { return res.json({ ok: false, message: 'A space from this host already exists at this location' }); }

            console.log('user obj data in space controller add:', succ)

            const done = await Spaces.create({
                locationAddress: locationAddress,
                locationCity: locationCity,
                locationCountry: locationCountry,
                capacity: Number(capacity),
                description: description,
                fotos: [],
                owner: succ._id,
                host: succ.name,
                email: succ.email,
                events: [],
                price: Number(price),
                type: type,
                timeOfDay: timeOfDay,
            });
            res.send({ ok: true, message: `Space created under user ${succ.email}` })
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

    //! (DELETE) DELETE SPACE
    async delete(req, res) {
        console.log('Space removed from database')
        let { _id } = req.params;
        try {
            const removed = await Spaces.deleteOne({ _id });
            res.send({ message: 'Space removed from database', removed });
        }
        catch (error) {
            console.log(error)
            res.send({ message: 'Error removing space from database', error });
        };
    }

    //! (PUT) UPDATE SPACE
    async update(req, res) {
        let { _id } = req.body;
        delete req.body._id

        console.log('UPDATE CONTROLLER', req.body)

        try {
            const updated = await Spaces.updateOne(
                { _id }, req.body
            );
            res.send({ message: 'Space updated in database', updated });
        }
        catch (error) {
            console.log(error)
            res.send({ message: 'Error updating space', error });
        };
    }

    //! (GET) FIND ALL SPACES
    async findAll(req, res) {
        try {
            const spaces = await Spaces.find({});
            res.json({ ok: true, message: `Your spaces:`, spaces });
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

    //! (POST) FIND FILTERED SPACES BY HOST
    async findBy(req, res) {
        try {
            const { _id } = req.body;
            const token = req.headers.authorization;
            const succ = await verify_token(token);
            if (!succ) { return res.json({ ok: false, message: `You can't see these spaces, sorry!` }) }

            const spaces = await Spaces.find({ owner: succ.admin && _id ? _id : succ._id });

            const ids = spaces.map(space => space._id)
            console.log(ids)
            const photos = await Images.find({ typeId: { $in: ids } })
            res.json({ ok: true, message: `Your spaces:`, spaces, photos })
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

    //! (GET) FIND SINGLE SPACE - for link sharing/reservation processes
    async findOne(req, res) {
        try {
            const space = await Spaces.findOne({ _id: req.params._id });

            const photos = await Images.find({ typeId: space._id })
            console.log('photos findOne', photos)
            res.json({ ok: true, message: `Your spaces:`, space, photos })
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

    //! (GET) FIND LOCATIONS (TO DISPLAY IN HOME-SEARCH)
    async findLocations(req, res) {
        const locations = []
        try {
            const spaces = await Spaces.find({});
            spaces.map((space, idx) => {
                if (!locations.includes(space.locationCity)) {
                    locations.push(space.locationCity)
                }
            });


            res.json({ ok: true, message: `Unique locations from findlocations with photos:`, locations });
        }
        catch (e) {
            console.log(e)
            res.send({ e })
        }
    }

}

module.exports = new SpacesControllers();