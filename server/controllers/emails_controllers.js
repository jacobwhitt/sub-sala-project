const space = require('../models/space_model')
const nodemailer = require('nodemailer')
// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
    // you need to enable the less secure option on your gmail account
    // https://myaccount.google.com/lesssecureapps?pli=1
    service: 'Gmail',
    auth: {
        user: process.env.NODEMAILER_EMAIL,
        pass: process.env.NODEMAILER_PASSWORD,
    }
});

//! EMAIL FORM VAR HANDLING/SENDING FUNCTION
const send_email = async (req, res) => {
    const { name, email, subject, message } = req.body
    const default_subject = 'This is a default subject'
    const mailOptions = {
        to: process.env.NODEMAILER_EMAIL,
        subject: 'Message from SubSala user: ' + email,
        html: '<p><pre>' + message + '</pre></p>'
    }
    try {
        if (!name) {
            return res.json({ ok: false, message: 'You forgot your name!' })
        }
        if (!email) {
            return res.json({ ok: false, message: 'You forgot an email address!' })
        }
        if (!subject) {
            return res.json({ ok: false, message: 'You forgot a subject!' })
        }
        if (!message) {
            return res.json({ ok: false, message: 'Did you forget to write the message? ;)' })
        }
        const response = await transport.sendMail(mailOptions)
        console.log('=========================================> Email sent !!')
        return res.json({ ok: true, message: 'Email sent to site admin correctly' })
    }
    catch (err) {
        return res.json({ ok: false, message: err })
    }
}

const confirmation_email = async (req, res) => {
    const { email, invoiceNum, reservationInfo } = req.body
    const default_subject = 'Message from SubSala'
    const thisSpace = await space.findOne({ _id: reservationInfo.spaceId })

    console.log('in email func thisSpace', thisSpace)
    console.log('space inside email before sending', email, thisSpace.host, thisSpace.locationAddress, thisSpace.email)

    const mailOptionsUser = {
        to: email,
        subject: 'Message from SubSala user: ' + email,
        html: '<p><pre>' + `This is a confirmation email containing the details of your reservation on SubSala!\n\nReservation location ${thisSpace.locationAddress}\n\nDate: ${reservationInfo.date}.\n\nInvoice number: ${invoiceNum}.\n\nWe hope you enjoy your event!\nThank you for using our service!\n\n—SubSala Admin Team` + '</pre></p>'
    }
    const mailOptionsHost = {
        to: email,
        subject: 'Message from SubSala user: ' + email,
        html: '<p><pre>' + `This is a confirmation email containing the details of a reservation made at your space on SubSala!\n\nUser with email: ${email} made a reservation at your space at ${thisSpace.locationAddress} on ${reservationInfo.date}.\n\nInvoice number: ${invoiceNum}.\n\nThank you for using our service!\n\n—SubSala Admin Team` + '</pre></p>'
    }

    const response = await transport.sendMail(mailOptionsUser)
    const response2 = await transport.sendMail(mailOptionsHost)
    console.log('==> Email sent !!')
}

module.exports = { send_email, confirmation_email }


