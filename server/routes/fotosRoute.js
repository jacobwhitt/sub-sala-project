const router = require("express").Router();
const controller = require("../controllers/fotos_controller");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public");
  },
  filename: function (req, file, cb) {
    console.log('ub storage function in server', file)
    cb(null, Date.now() + "-" + file.originalname);
  }
});
const upload = multer({ storage: storage }).array("file");



router.post("/fetchspaceimages", controller.fetchSpaceImages);
router.post("/fetchimagesforhost/:_id", controller.fetchImagesForHost);
router.delete("/delete_image/:_id/:filename/:spaceid", controller.deleteImage);
router.post("/upload/:_id/:type", upload, async (req, res) => {
  console.log("req.body from fotoRoutes =======>", req.files);
  return await controller.uploadImage(req, res, upload, req.files)
});

module.exports = router;