const router = require('express').Router();
const controller = require('../controllers/users.controllers')

router.post('/register', controller.register);
router.post('/login', controller.login);
router.post('/verify_token', controller.verify_token);
router.put('/update', controller.update)
router.get('/findone', controller.findOne);
router.get('/findall', controller.findAll);
router.delete('/delete/:_id', controller.delete);

module.exports = router;