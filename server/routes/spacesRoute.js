const express = require('express');
const router = express.Router();
const controller = require('../controllers/spaces_controllers')

router.post('/create', controller.create)
router.delete('/delete/:_id', controller.delete)
router.put('/update', controller.update)
router.get('/findall', controller.findAll)
router.post('/findby', controller.findBy)
router.get('/findone/:_id', controller.findOne)
router.get('/findlocations', controller.findLocations)

module.exports = router;