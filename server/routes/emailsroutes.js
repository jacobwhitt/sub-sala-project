const router = require('express').Router();
const controller = require('../controllers/emails_controllers.js')

router.post('/send_email', controller.send_email)
router.post('/confirmation_email', controller.confirmation_email)

module.exports = router