const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    path = require('path');
require("dotenv").config();

var PORT = process.env.PORT || 3002

// =================== initial settings ===================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting() {
    try {
        await mongoose.connect(process.env.ATLASURL, { useUnifiedTopology: true, useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch (error) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes

app.use('/spaces', require('./routes/spacesRoute'));
app.use('/fotos', require('./routes/fotosRoute'));
app.use('/users', require('./routes/usersroutes'))
app.use('/emails', require('./routes/emailsroutes'))
app.use("/payment", require('./routes/paymentroute'));

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.use('/assets', express.static(path.join(__dirname, './public')))

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});
app.listen(PORT, function () {
    console.log(`Serving my master on port ${PORT}!`)
})