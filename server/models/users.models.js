const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    admin: { type: Boolean, required: true },
    host: { type: Boolean, required: true }
});
module.exports = mongoose.model('users', userSchema);