//! SPACE SCHEMA
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const spaceSchema = new Schema({
    locationAddress: {
        type: String,
        required: true
    },

    locationCity: {
        type: String,
        required: true
    },

    locationCountry: {
        type: String,
        required: true
    },

    capacity: {
        type: Number,
        required: true,
    },

    description: {
        type: String,
        required: true
    },

    fotos: {
        type: Array,
        required: true
    },

    owner: {
        type: String,
        required: true
    },

    host: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
    },

    events: { //fetches airbnb.io calendar API 'DayPickerRangeController'
        type: Array,
        required: true,
    },

    price: {
        type: Number,
        required: true
    },

    type: { //array of types of events possible
        type: Array,
        required: true
    },

    timeOfDay: { //array with day/night
        type: Array,
        required: true
    },
})

module.exports = mongoose.model('space', spaceSchema);