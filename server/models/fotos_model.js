const mongoose = require("mongoose");

const imagesSchema = new mongoose.Schema({
  original: {
    type: String,
    default: ''
  },
  thumbnail: {
    type: String,
    default: ''
  },
  pathname: {
    type: String,
    required: true
  },
  filename: {
    type: String,
    required: true
  },
  typeId: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("images", imagesSchema);
